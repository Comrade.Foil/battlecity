#include "utils.h"


using namespace cnst;

Vec2 operator+(Vec2 const &p1, Vec2 const &p2) {
	Vec2 v;
	v.x = p1.x + p2.x;
	v.y = p1.y + p2.y;
	return v;
}
Vec2 operator+(Vec2 const &p1, Vec2i const &p2) {
	Vec2 v;
	v.x = p1.x + p2.x;
	v.y = p1.y + p2.y;
	return v;
}
Vec2 operator+(Vec2i const &p1, Vec2 const &p2) {
	Vec2 v;
	v.x = p1.x + p2.x;
	v.y = p1.y + p2.y;
	return v;
}


Vec2 operator-(Vec2 const &p1, Vec2 const &p2) {
	Vec2 v;
	v.x = p1.x - p2.x;
	v.y = p1.y - p2.y;
	return v;
}
Vec2 operator-(Vec2 const &p1, Vec2i const &p2) {
	Vec2 v;
	v.x = p1.x - p2.x;
	v.y = p1.y - p2.y;
	return v;
}
Vec2 operator-(Vec2i const &p1, Vec2 const &p2) {
	Vec2 v;
	v.x = p1.x - p2.x;
	v.y = p1.y - p2.y;
	return v;
}

bool operator==(const Vec2 &p1, const Vec2 &p2) { return p1.x == p2.x && p1.y == p2.y; }
Vec2 operator*(Vec2 const &p1, double const &scalar) { return {p1.x * scalar, p1.y * scalar }; }

Vec2i operator+(Vec2i const &p1, Vec2i const &p2) { return {p1.x + p2.x, p1.y + p2.y}; }
Vec2i operator-(Vec2i const &p1, Vec2i const &p2) { return {p1.x - p2.x, p1.y - p2.y}; }
bool operator==(const Vec2i &p1, const Vec2i &p2) { return p1.x == p2.x && p1.y == p2.y; }
Vec2 operator*(Vec2i const &p1, double const &scalar) {
	Vec2 v;
	v.x = p1.x * scalar;
	v.y = p1.y * scalar;
	return v;
}

Rect operator+(Rect const &r, Vec2 const &p) { return {r.x + p.x, r.y + p.y, r.w, r.h}; };

double vecLength(Vec2 const &vec) {
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

Vec2 vecAbs(const Vec2 &vec) {
	return {abs(vec.x), abs(vec.y)};
}

EntityID getNewID() {
	static EntityID id = 0;
	return id++;
}

int sgn(int val) {
	return signbit(val) ? -1 : 1;
}

int randomInRange(int min, int max) {
	return min + rand()%(max+1-min);
}

Vec2i getTileToWorldPosition(int x, int y) {
	return Vec2i{(int)(x * TILE_WIDTH), (int)(y * TILE_WIDTH)};
}

Vec2i getWorldToTilePosition(double x, double y) {
	int _x = (int)x / TILE_WIDTH;
	int _y = (int)y / TILE_WIDTH;
	return Vec2i{_x, _y};
}

bool isOutOfBoundsTile(int x, int y) {
	return x < 0 || y < 0 || x > TILES_NUM_X-1 || y > TILES_NUM_Y-1;
}

double clamp(double x, double _min, double _max) {
	return max(min(x, _max), _min);
}

Vec2 clamp(Vec2 v, Vec2 _min, Vec2 _max) {
	return Vec2{clamp(v.x, _min.x, _max.x), clamp(v.y, _min.y, _max.y)};
}