#ifndef BATTLECITY_MATH_H
#define BATTLECITY_MATH_H

#include "math.h"
#include <unordered_map>
#include <string>
#include <cstdlib>
#include "../src/globals.h"
#include "types.h"

typedef unsigned int EntityID;

Vec2 operator+(const Vec2 &p1, const Vec2 &p2);
Vec2 operator+(const Vec2 &p1, const Vec2i &p2);
Vec2 operator+(const Vec2i &p1, const Vec2 &p2);

Vec2 operator-(const Vec2 &p1, const Vec2 &p2);
Vec2 operator-(const Vec2 &p1, const Vec2i &p2);
Vec2 operator-(const Vec2i &p1, const Vec2 &p2);

bool operator==(const Vec2 &p1, const Vec2 &p2);
Vec2 operator*(const Vec2 &p1, const double &scalar);
Vec2 operator*(Vec2i const &p1, double const &scalar);

Vec2i operator+(Vec2i const &p1, Vec2i const &p2);
Vec2i operator-(Vec2i const &p1, Vec2i const &p2);
bool operator==(const Vec2i &p1, const Vec2i &p2);


double vecLength(const Vec2 &vec);
Vec2 vecAbs(const Vec2 &vec);
int randomInRange(int min, int max);
Vec2i getTileToWorldPosition(int x, int y);
Vec2i getWorldToTilePosition(double x, double y);
double clamp(double x, double min, double max);
Vec2 clamp(Vec2 v, Vec2 min, Vec2 max);
bool isOutOfBoundsTile(int x, int y);
EntityID getNewID();
int sgn(int val);


#endif //BATTLECITY_MATH_H
