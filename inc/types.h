#ifndef BATTLECITY_TYPES_H
#define BATTLECITY_TYPES_H

#include <unordered_map>
#include <map>
#include <string>
#include "Framework.h"


typedef unsigned int EntityID;

struct Vec2 {
	double x;
	double y;
};

struct Vec2i {
	int x;
	int y;
};

struct Rect {
	double x;
	double y;
	double w;
	double h;
	Rect operator+(const Vec2 &p) {
		Rect r;
		r.x = x + p.x;
		r.y = y + p.y;
		r.w = w;
		r.h = h;
		return r;
	}
	Rect& operator+=(const Vec2 &p) {
		x = x + p.x;
		y = x + p.y;
		return *this;
	}
	
	Rect& operator=(const Rect& r) = default;
};

struct IPointCompare
{
	size_t operator()(const Vec2i& k)const
	{
		return std::hash<double>()(k.x) ^ std::hash<double>()(k.y);
	}
	
	bool operator()(const Vec2i& a, const Vec2i& b)const
	{
		return a.x == b.x && a.y == b.y;
	}
};

typedef std::unordered_map<Vec2i, int, IPointCompare, IPointCompare> DirIndexMap_t;
typedef std::unordered_map<Vec2i, std::string, IPointCompare, IPointCompare> DirStringMap_t;
typedef std::unordered_map<Vec2i, Vec2i, IPointCompare, IPointCompare> VecVecMap_t;
typedef std::map<FRKey, Vec2i> KeyDirectionMap_t;


#endif //BATTLECITY_TYPES_H
