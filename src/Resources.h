#ifndef BATTLECITY_RESOURCES_H
#define BATTLECITY_RESOURCES_H

#include "Framework.h"
#include "globals.h"
#include <unordered_map>
#include <iostream>
#include <string>

using namespace std;

class Resources {
	unordered_map<string, Sprite*> spriteResources;
	
public:
	void initResources() {
		for (auto it = cnst::SPRITES_CONFIG.begin(); it != cnst::SPRITES_CONFIG.end(); it++) {
			spriteResources[it->first] = createSprite(it->second.c_str());
		}
	}
	
	Sprite* getSprite(string spriteName) {
		auto it = spriteResources.find(spriteName);
		if (it != spriteResources.end()) {
			return it->second;
		} else {
			cout << "Didn't find sprite: " << spriteName << endl;
			return nullptr;
		}
	}
	
	void destroy() {
		for (auto it = spriteResources.begin(); it != spriteResources.end(); it++) {
			destroySprite(spriteResources[it->first]);
		}
		spriteResources.clear();
	}
};


#endif //BATTLECITY_RESOURCES_H
