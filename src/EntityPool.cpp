#include "EntityPool.h"


EntityPool::EntityPool(Resources &res) {
	this->res = &res;
}

void EntityPool::addEntity(shared_ptr<GameEntity> entity) {
	this->data[entity->id] = entity;
}

void EntityPool::removeEntity(const EntityID &id) {
	auto it = data.find(id);
	
	if (it == data.end()) {
#ifdef DEBUG_INFO
		std::cout << "Cannot remove entity with id " << id << std::endl;
#endif
	} else {
		it->second.reset();
		data.erase(it->first);
		
	}
}

shared_ptr<GameEntity> EntityPool::getEntity(const EntityID &id) {
	auto it = this->data.find(id);
	
	if (it == this->data.end()) {
#ifdef DEBUG_INFO
		std::cout << "Didn't find entity with id " << id << std::endl;
#endif
		return nullptr;
	} else {
		return it->second;
	}
}

void EntityPool::update(const unsigned int &dTime) {
	auto it = data.begin();
	
	while (it != data.end()) {
		it->second->update(dTime);
		it++;
	}
}

void EntityPool::updateCollisionGrid() {
	for (int i = 0; i < TILES_NUM_X; i++) {
		for (int j = 0; j < TILES_NUM_Y; j++) {
			vector<EntityID>().swap(collisionGrid[i][j]);
		}
	}
	
	auto it = data.begin();
	while (it != data.end()) {
		GameEntity* entity = it->second.get();
		if (!entity->removed) {
			Vec2i tile = getWorldToTilePosition(entity->boundingBox.x, entity->boundingBox.y);
			this->collisionGrid[tile.x][tile.y].push_back(entity->id);
		}
		
		it++;
	}
}

vector<EntityID> EntityPool::getNeighbours(const EntityID &id) {
	vector<EntityID> neighbours;
	GameEntity* entity = this->data[id].get();
	Vec2i tile = getWorldToTilePosition(entity->boundingBox.x, entity->boundingBox.y);
	
	for (int i = -1; i <= 1; i++) {
		for (int j = -1; j <= 1; j++) {
			int _x = tile.x + j;
			int _y = tile.y + i;
			if(isOutOfBoundsTile(_x, _y)) continue;

			for (auto it = this->collisionGrid[_x][_y].begin(); it != this->collisionGrid[_x][_y].end(); it++) {
				if (*it != id) {
					neighbours.push_back(*it);
				}
			}
		}
	}
	return neighbours;
}

vector<Vec2i> EntityPool::getFreeCells() {
	vector<Vec2i> freeCells;
	
	for (int i = 0; i < TILES_NUM_X; i++) {
		for (int j = 0; j < TILES_NUM_Y; j++) {
			if (this->collisionGrid[i][j].empty()) {
				freeCells.push_back(Vec2i{i, j});
			}
		}
	}
	
	return freeCells;
}

vector<EntityID> EntityPool::getCellDwellers(int x, int y) {
	return this->collisionGrid[x][y];
}

void EntityPool::draw() {
	auto it = data.begin();
	
	while (it != data.end()) {
		it->second->draw();
		it++;
	}
}

void EntityPool::destroyRemovedEntities() {
	auto it = data.begin();
	
	while (it != data.end()) {
		if (it->second->removed) {
			shared_ptr<GameEntity> e = it->second;
			if (e->type & (B_WALL | TANK)) {
				addEntity(make_shared<Animation>(*res, (int)e->boundingBox.x, (int)e->boundingBox.y, EXPLOSION_ANIM, 300));
			}
			it->second.reset();
			data.erase(it++);
		} else {
			++it;
		}
	}
}

void EntityPool::destroy() {
	auto it = data.begin();
	
	while (it != data.end()) {
		it->second.reset();
		data.erase(it++);
	}
	
	data.clear();
}

EntityPool::~EntityPool() {
	destroy();
}