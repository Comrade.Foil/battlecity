#include "AiManager.h"

AiManager::AiManager(Resources &res, EntityPool &ep, unsigned int aiMaxAmount, unsigned int tanksLeft) {
	this->aiMaxAmount = aiMaxAmount;
	this->tanksLeft = tanksLeft;
	this->ep = &ep;
	this->res = &res;
	this->tickCounter = AI_SPAWN_DELAY;
}

void AiManager::update(unsigned int dTime) {
	auto it = ais.begin();
	
	while(it != ais.end()) {
		if ((*it)->isRemoved()) {
			it->reset();
			ais.erase(it);
			tickCounter = 0;
		} else {
			(*it)->update(dTime);
			++it;
		}
	}
	
	if (ais.size() < aiMaxAmount && tanksLeft > 0 && tickCounter >= AI_SPAWN_DELAY) {
		spawnAiOnRandomTile();
		tickCounter = 0;
	}
	
	this->tickCounter += dTime;
	
}

bool AiManager::isDead() {
	return tanksLeft == 0 && ais.empty();
}

void AiManager::spawnAiOnRandomTile() {
	if (tanksLeft == 0) {
		return;
	}
	vector<Vec2i> freeTiles = this->ep->getFreeCells();
	Vec2i randomTile = freeTiles[randomInRange(0, (int)freeTiles.size()-1)];
	Vec2i worldPosition = getTileToWorldPosition(randomTile.x, randomTile.y);
	shared_ptr<Ai> ai = make_shared<Ai>(*res, *ep, (unsigned int)worldPosition.x, (unsigned int)worldPosition.y);
	this->ais.push_back(ai);
	this->ep->addEntity(ai->tank);
	this->ep->updateCollisionGrid();
	tanksLeft--;
	cout << "Enemy tanks left: " << tanksLeft << endl;
}

AiManager::~AiManager() {
	ais.clear();
}