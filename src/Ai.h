#ifndef BATTLECITY_AI_H
#define BATTLECITY_AI_H

#include <vector>
#include <map>
#include <iostream>
#include "math.h"
#include <time.h>
#include "globals.h"
#include "utils.h"
#include "EntityPool.h"
#include "Entities/Tank.h"

using namespace std;
using namespace cnst;

class Ai {
	unsigned int tickCounter;
	bool isFreeCell(unsigned int x, unsigned int y);
public:
	Resources *res;
	EntityPool *ep;
	Vec2i destination;
	shared_ptr<Tank> tank;
	
	Ai() = default;
	Ai(Resources &res, EntityPool &ep, unsigned int x, unsigned int y);
	~Ai() = default;
	bool update(const unsigned int &dTime);

	Vec2i chooseNextDestination(Vec2i offset);
	Vec2i chooseNextDirection();
	
	bool isRemoved();
};


#endif //BATTLECITY_AI_H
