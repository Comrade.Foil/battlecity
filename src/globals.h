#ifndef BATTLECITY_GLOBALS_H
#define BATTLECITY_GLOBALS_H

#include <string>
#include "utils.h"
#include "types.h"
#include <vector>
#include <unordered_map>

// #define DEBUG_INFO

using namespace std;

namespace cnst {
	const DirIndexMap_t DIR_INDEX_MAP = {
		{{1, 0}, 0},
		{{-1, 0}, 1},
		{{0, 1}, 2},
		{{0, -1}, 3},
	};
	
	const DirStringMap_t DIR_STRING_MAP = {
		{{1, 0}, "right"},
		{{-1, 0}, "left"},
		{{0, 1}, "down"},
		{{0, -1}, "up"},
	};
	
	const unordered_map<string, Vec2> STRING_DIR_MAP = {
		{"right", {1, 0}},
		{"left", {-1, 0}},
		{"down", {0, 1}},
		{"up", {0, -1}},
	};
	
	const unordered_map<string, string> SPRITES_CONFIG = {
		{"yellow-up", "data/yellow-up.png"},
		{"yellow-down", "data/yellow-down.png"},
		{"yellow-left", "data/yellow-left.png"},
		{"yellow-right", "data/yellow-right.png"},
		
		{"green-up", "data/green-up.png"},
		{"green-down", "data/green-down.png"},
		{"green-left", "data/green-left.png"},
		{"green-right", "data/green-right.png"},
		
		{"brick-wall", "data/brick-wall.png"},
		{"metal-wall", "data/metal-wall.png"},
		
		{"bullet-up", "data/bullet-up.png"},
		{"bullet-down", "data/bullet-down.png"},
		{"bullet-left", "data/bullet-left.png"},
		{"bullet-right", "data/bullet-right.png"},
		
		{"eagle", "data/eagle.png"},
		{"eagle-dead", "data/eagle-dead.png"},
		
		{"explosion-1", "data/explosion-1.png"},
		{"explosion-2", "data/explosion-2.png"},
		{"explosion-3", "data/explosion-3.png"},
		
		{"spawn-1", "data/spawn-1.png"},
		{"spawn-2", "data/spawn-2.png"},
		{"spawn-3", "data/spawn-3.png"},
		{"spawn-4", "data/spawn-4.png"},
		
	};
	
	const vector<string> SPAWN_ANIM {
		"spawn-1", "spawn-2", "spawn-3","spawn-4"
	};
	
	const vector<string> EXPLOSION_ANIM {
		"explosion-1", "explosion-2", "explosion-3"
	};
	
	enum EntityType {
		NONE_W = (0x00),
		TANK = (0x01),
		BULLET = (0x01) << 1,
		B_WALL = (0x01) << 2,
		M_WALL = (0x01) << 3,
		EAGLE = (0x01) << 4,
		EFFECT = (0x01) << 5,
	};
	
	enum EntityRole {
		NONE,
		PLAYER,
		ENEMY
	};
	
	const int TANK_WIDTH = 16;
	const int TANK_HEIGHT = 16;
	const int BULLET_WIDTH = 4;
	const int BULLET_HEIGHT = 4;
	
	const double SNAPPING_THRESHOLD = sqrt(265);
	
	const VecVecMap_t BULLET_DIRECTION_OFFSET_MAP = {
		{{1, 0}, {0, -BULLET_HEIGHT/2}},
		{{-1, 0}, {-BULLET_WIDTH, -BULLET_HEIGHT/2}},
		{{0, 1}, {-BULLET_WIDTH/2, 0}},
		{{0, -1}, {-BULLET_WIDTH/2, -BULLET_HEIGHT}},
	};
	
	const unsigned int MS_PER_ANIMATION_FRAME = 80;
	
	const unsigned int TILES_NUM_X = 13;
	const unsigned int TILES_NUM_Y = 13;
	const unsigned int TILE_WIDTH = 16;
	const unsigned int PLAYER_TANKS_NUM = 4;
	const unsigned int AI_FIRE_LIMITER = 800;
	const unsigned int AI_SPAWN_DELAY = 1000;
	const unsigned int AI_MAX_AMOUNT = 4;
	const unsigned int AI_TANKS_LEFT = 8;
	
	const int WORLD_SIZE = TILES_NUM_X * TILE_WIDTH;
	
	const EntityType MAP[TILES_NUM_X][TILES_NUM_Y] = {
		{NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W},
		{NONE_W, B_WALL, NONE_W, M_WALL, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, M_WALL, NONE_W, B_WALL, NONE_W},
		{NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W},
		{NONE_W, B_WALL, B_WALL, M_WALL, NONE_W, B_WALL, M_WALL, B_WALL, NONE_W, M_WALL, B_WALL, B_WALL, NONE_W},
		{NONE_W, B_WALL, NONE_W, B_WALL, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, B_WALL, NONE_W, B_WALL, NONE_W},
		{NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W},
		{NONE_W, B_WALL, NONE_W, M_WALL, NONE_W, B_WALL, B_WALL, B_WALL, NONE_W, M_WALL, NONE_W, B_WALL, NONE_W},
		{NONE_W, B_WALL, NONE_W, NONE_W, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, NONE_W, NONE_W, B_WALL, NONE_W},
		{NONE_W, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, B_WALL, NONE_W, NONE_W, NONE_W},
		{NONE_W, B_WALL, NONE_W, B_WALL, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, B_WALL, NONE_W, B_WALL, NONE_W},
		{NONE_W, B_WALL, NONE_W, B_WALL, NONE_W, M_WALL, B_WALL, M_WALL, NONE_W, B_WALL, NONE_W, B_WALL, NONE_W},
		{NONE_W, NONE_W, NONE_W, NONE_W, NONE_W, B_WALL, B_WALL, B_WALL, NONE_W, NONE_W, NONE_W, NONE_W, NONE_W},
		{NONE_W, M_WALL, NONE_W, B_WALL, NONE_W, B_WALL, EAGLE, B_WALL, NONE_W, B_WALL, NONE_W, M_WALL, NONE_W},
	};
}

#endif //BATTLECITY_GLOBALS_H
