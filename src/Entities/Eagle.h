#ifndef BATTLECITY_EAGLE_H
#define BATTLECITY_EAGLE_H

#include "GameEntity.h"
#include "Framework.h"
#include "SDL2/SDL.h"
#include "../Resources.h"

class Eagle : public GameEntity {
	Sprite *activeSprite;
public:
	Eagle(Resources &resources);
	Eagle(Resources &resources, int x, int y);
	~Eagle();
	void draw();
	void update(unsigned int dTime);
	void onHit(EntityType entityType, EntityRole entityRole);
};


#endif //BATTLECITY_EAGLE_H
