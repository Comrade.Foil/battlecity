#ifndef BATTLECITY_ENTITY_H
#define BATTLECITY_ENTITY_H

#include "Framework.h"
#include "GameEntity.h"
#include "utils.h"
#include "../globals.h"
#include "../Resources.h"
#include "../EntityPool.h"
#include <unordered_map>
#include <string>

using namespace std;

class Tank : public GameEntity {
	Sprite *sprites[4];
	Sprite *activeSprite;
	EntityID bulletId;
	
	void updateBoundingBox();
public:
	double moveSpeed;
	int bulletSpeed;
	bool moving;
	
	
	Tank(Resources &resources, string color, EntityRole role);
	Tank(Resources &resources, string color, EntityRole role, int x, int y);
	
	void draw();
	void update(unsigned int dTime);
	void fire(Resources &res, EntityPool &ep);
	void onHit(EntityType entityType, EntityRole entityRole);
	
	~Tank();
};


#endif //BATTLECITY_ENTITY_H
