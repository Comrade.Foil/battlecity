#include "BrickWall.h"

BrickWall::BrickWall(Resources &resources) {
	this->type = B_WALL;
	this->role = NONE;
	this->health = 2;
	
	this->activeSprite = resources.getSprite("brick-wall");
	this->boundingBox = {0, 0, TILE_WIDTH, TILE_WIDTH};
}

BrickWall::BrickWall(Resources &resources, int x, int y) {
	this->type = B_WALL;
	this->role = NONE;
	this->health = 2;
	
	this->activeSprite = resources.getSprite("brick-wall");
	this->boundingBox = {(double)x, (double)y, TILE_WIDTH, TILE_WIDTH};
}

void BrickWall::draw() {
	drawSprite(activeSprite, boundingBox.x, boundingBox.y);
}

void BrickWall::update(unsigned int dTime) {

}

void BrickWall::onHit(EntityType entityType, EntityRole entityRole) {
	if (entityType == BULLET) {
		health--;
		if (health == 0) {
			removed = true;
		}
	}
}

BrickWall::~BrickWall() {
#ifdef DEBUG_INFO
	cout << "deleting brick wall" << endl;
#endif
}