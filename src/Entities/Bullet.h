#ifndef BATTLECITY_BULLET_H
#define BATTLECITY_BULLET_H

#include "Framework.h"
#include "SDL2/SDL.h"
#include "GameEntity.h"
#include "utils.h"
#include "../globals.h"
#include "../Resources.h"
#include <unordered_map>
#include <string>

class Bullet : public GameEntity {
	Sprite *activeSprite;
public:
	double moveSpeed;
	
	Bullet(Resources &resources, EntityRole role, Vec2 pos, Vec2i dir);
	
	void draw();
	void update(unsigned int dTime);
	void onHit(EntityType entityType, EntityRole entityRole);
	bool isOutsideMap();
	
	~Bullet();
};


#endif //BATTLECITY_BULLET_H
