#ifndef BATTLECITY_GAMEENTITY_H
#define BATTLECITY_GAMEENTITY_H


#include <iostream>
#include "utils.h"
#include "../globals.h"

using namespace cnst;

class GameEntity {
public:
	EntityID id;
	EntityRole role;
	bool removed;
	EntityType type;
	Rect boundingBox;
	Vec2i direction;
	
	GameEntity() {
		removed = false;
		id = getNewID();
	}
	virtual void draw() = 0;
	virtual void update(unsigned int dTime) = 0;
	virtual ~GameEntity() = default;
	virtual void onHit(EntityType entityType, EntityRole entityRole) = 0;
	Vec2 getCenter() {
		return Vec2{
			boundingBox.x + boundingBox.w/2,
			boundingBox.y + boundingBox.h/2
		};
	};
};


#endif //BATTLECITY_GAMEENTITY_H
