#include "Eagle.h"

Eagle::Eagle(Resources &resources) {
	this->type = EntityType::EAGLE;
	
	this->activeSprite = resources.getSprite("eagle");
	this->boundingBox = {0, 0, TILE_WIDTH, TILE_WIDTH};
}

Eagle::Eagle(Resources &resources, int x, int y) {
	this->type = EntityType::EAGLE;
	
	this->activeSprite = resources.getSprite("eagle");
	this->boundingBox = {(double)x, (double)y, TILE_WIDTH, TILE_WIDTH};
}

void Eagle::draw() {
	drawSprite(activeSprite, boundingBox.x, boundingBox.y);
}

void Eagle::update(unsigned int dTime) {

}

void Eagle::onHit(EntityType entityType, EntityRole entityRole) {
	if (entityType == BULLET) {
		removed = true;
	}
}

Eagle::~Eagle() {
#ifdef DEBUG_INFO
	cout << "deleting brick wall" << endl;
#endif
}