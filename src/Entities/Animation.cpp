#include "Animation.h"

Animation::Animation(Resources &res, int x, int y, const vector<string> &animFrames) {
	for (auto frame : animFrames) {
		frames.push_back(res.getSprite(frame));
	}
	
	this->boundingBox.x = x;
	this->boundingBox.y = y;
	this->boundingBox.w = TILE_WIDTH;
	this->boundingBox.h = TILE_WIDTH;
	
	this->type = EFFECT;
	this->tickCounter = 0;
	this->framesNum = (unsigned int)frames.size();
	this->lifeTime = -1;
}

Animation::Animation(Resources &res, int x, int y, const vector<string> &animFrames, int lifeTime) {
	for (auto frame : animFrames) {
		frames.push_back(res.getSprite(frame));
	}
	
	this->boundingBox.x = x;
	this->boundingBox.y = y;
	this->boundingBox.w = TILE_WIDTH;
	this->boundingBox.h = TILE_WIDTH;
	
	this->type = EFFECT;
	this->tickCounter = 0;
	this->framesNum = (unsigned int)frames.size();
	this->lifeTime = lifeTime;
}

void Animation::update(unsigned int dTime) {
	unsigned int frameIndex = (tickCounter / MS_PER_ANIMATION_FRAME) % framesNum;
	activeSprite = frames[frameIndex];
	tickCounter += dTime;
	
	if (this->tickCounter > lifeTime) {
		removed = true;
	}
}

void Animation::onHit(EntityType entityType, EntityRole entityRole) {

}

void Animation::draw() {
	drawSprite(activeSprite, (int)this->boundingBox.x, (int)this->boundingBox.y);
}

Animation::~Animation() {
	vector<Sprite*>().swap(frames);
}