#include "Bullet.h"


Bullet::Bullet(Resources &resources, EntityRole role, Vec2 pos, Vec2i dir) {
	this->type = EntityType::BULLET;
	this->role = role;
	
	auto it = cnst::DIR_STRING_MAP.find(dir);
	string direction = "up";
	if (it != cnst::DIR_STRING_MAP.end()) {
		direction = it->second;
	}
	
	this->activeSprite = resources.getSprite("bullet-" + direction);
	this->moveSpeed = 1.0/8.0;
	this->direction = dir;
	
	this->boundingBox.x = pos.x;
	this->boundingBox.y = pos.y;
	this->boundingBox.w = cnst::BULLET_WIDTH;
	this->boundingBox.h = cnst::BULLET_HEIGHT;
	
}

void Bullet::draw() {
	drawSprite(this->activeSprite, this->boundingBox.x, this->boundingBox.y);
}

void Bullet::update(unsigned int dTime) {
	boundingBox = boundingBox + direction * moveSpeed * dTime;
	if (isOutsideMap()) {
		this->removed = true;
	}
}

bool Bullet::isOutsideMap() {
	double x = this->boundingBox.x;
	double y = this->boundingBox.y;
	return (x <= 0 || x >= WORLD_SIZE || y <= 0 || y >= WORLD_SIZE);
}

void Bullet::onHit(EntityType entityType, EntityRole entityRole) {
	if (entityRole != role) {
		removed = true;
	}
}

Bullet::~Bullet() {

}