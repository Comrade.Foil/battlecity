#ifndef BATTLECITY_BRICKWALL_H
#define BATTLECITY_BRICKWALL_H


#include "GameEntity.h"
#include "Framework.h"
#include "SDL2/SDL.h"
#include "../Resources.h"

class BrickWall : public GameEntity {
	unsigned int health;
	Sprite *activeSprite;
public:
	BrickWall(Resources &resources);
	BrickWall(Resources &resources, int x, int y);
	~BrickWall();
	void draw();
	void update(unsigned int dTime);
	void onHit(EntityType entityType, EntityRole entityRole);
};


#endif //BATTLECITY_BRICKWALL_H
