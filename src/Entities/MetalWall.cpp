#include "MetalWall.h"

MetalWall::MetalWall(Resources &resources) {
	this->type = M_WALL;
	this->role = NONE;
	
	this->activeSprite = resources.getSprite("metal-wall");
	this->boundingBox = {0, 0, TILE_WIDTH, TILE_WIDTH};
}

MetalWall::MetalWall(Resources &resources, int x, int y) {
	this->type = M_WALL;
	this->role = NONE;
	
	this->activeSprite = resources.getSprite("metal-wall");
	this->boundingBox = {(double)x, (double)y, TILE_WIDTH, TILE_WIDTH};
}

void MetalWall::draw() {
	drawSprite(activeSprite, boundingBox.x, boundingBox.y);
}

void MetalWall::update(unsigned int dTime) {
}

void MetalWall::onHit(EntityType entityType, EntityRole entityRole) {

}

MetalWall::~MetalWall() {
#ifdef DEBUG_INFO
	cout << "deleting metal wall" << endl;
#endif
}