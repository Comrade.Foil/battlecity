#include "Tank.h"
#include "Bullet.h"


Tank::Tank(Resources &resources, string color, EntityRole role) {
	this->type = TANK;
	this->role = role;
	
	this->sprites[0] = resources.getSprite(color + "-right");
	this->sprites[1] = resources.getSprite(color + "-left");
	this->sprites[2] = resources.getSprite(color + "-down");;
	this->sprites[3] = resources.getSprite(color + "-up");;
	
	this->moveSpeed = 1.0/26.0;
	this->bulletSpeed = 1;
	this->moving = false;
	
	this->direction = {0, -1};
	
	this->boundingBox.x = 0;
	this->boundingBox.y = 0;
	this->boundingBox.w = cnst::TANK_WIDTH;
	this->boundingBox.h = cnst::TANK_HEIGHT;
	
	this->bulletId = -1;
}

Tank::Tank(Resources &resources, string color, EntityRole role, int x, int y) {
	this->type = TANK;
	this->role = role;
	
	this->sprites[0] = resources.getSprite(color + "-right");
	this->sprites[1] = resources.getSprite(color + "-left");
	this->sprites[2] = resources.getSprite(color + "-down");;
	this->sprites[3] = resources.getSprite(color + "-up");;
	
	this->moveSpeed = 1.0/20.0;
	this->bulletSpeed = 1;
	this->moving = false;
	
	this->direction = {0, -1};
	
	this->boundingBox.x = (double)x;
	this->boundingBox.y = (double)y;
	this->boundingBox.w = TANK_WIDTH;
	this->boundingBox.h = TANK_HEIGHT;
	
	this->bulletId = -1;
}

void Tank::draw() {
	auto it = DIR_INDEX_MAP.find({this->direction});
	if (it != DIR_INDEX_MAP.end()) {
		int spriteIndex = it->second;
		this->activeSprite = this->sprites[spriteIndex];
	}
	
	drawSprite(this->activeSprite, (int)this->boundingBox.x, (int)this->boundingBox.y);
}

void Tank::update(unsigned int dTime) {
	if (moving) {
		Vec2 currentPosition = Vec2{boundingBox.x, boundingBox.y};
		double diff = (moveSpeed * dTime);
		Vec2 newPosition = clamp(
			currentPosition + direction * diff,
			Vec2{0, 0},
			Vec2{WORLD_SIZE - TILE_WIDTH, WORLD_SIZE - TILE_WIDTH}
		);
		
		boundingBox.x = newPosition.x;
		boundingBox.y = newPosition.y;
	}
	moving = false;
}

void Tank::onHit(EntityType entityType, EntityRole entityRole) {
	if (entityType == BULLET && entityRole != role) {
		removed = true;
	}
}

void Tank::fire(Resources &res, EntityPool &ep) {
	if (ep.getEntity(bulletId) != nullptr) {
		return;
	}
	
	Vec2 halfTank = {TANK_WIDTH / 2, TANK_HEIGHT / 2};
	Vec2i bulletOffset = cnst::BULLET_DIRECTION_OFFSET_MAP.find(direction)->second;
	Vec2 firePosition = getCenter() + Vec2{direction.x * halfTank.x, direction.y * halfTank.y} + bulletOffset;
	shared_ptr<Bullet> bullet = make_shared<Bullet>(res, role, firePosition, direction);
	
	ep.addEntity(bullet);
	bulletId = bullet->id;
}

Tank::~Tank() {

}