#ifndef BATTLECITY_ANIMATION_H
#define BATTLECITY_ANIMATION_H

#include "Framework.h"
#include "GameEntity.h"
#include "utils.h"
#include "../globals.h"
#include "../Resources.h"
#include "../EntityPool.h"
#include <unordered_map>
#include <string>

class Animation : public GameEntity {
	vector<Sprite*> frames;
	Sprite *activeSprite;
	unsigned int framesNum;
	unsigned int tickCounter;
	int lifeTime;
public:
	Animation(Resources &res, int x, int y, const vector<string> &animFrames);
	Animation(Resources &res, int x, int y, const vector<string> &animFrames, int lifeTime);
	void update(unsigned int dTime);
	void draw();
	void onHit(EntityType entityType, EntityRole entityRole);
	~Animation();
};


#endif //BATTLECITY_ANIMATION_H
