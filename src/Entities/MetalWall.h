#ifndef BATTLECITY_IRONWALL_H
#define BATTLECITY_IRONWALL_H


#include "GameEntity.h"
#include "Framework.h"
#include "SDL2/SDL.h"
#include "../Resources.h"

class MetalWall : public GameEntity {
	Sprite *activeSprite;
public:
	MetalWall(Resources &resources);
	MetalWall(Resources &resources, int x, int y);
	~MetalWall();
	void draw();
	void update(unsigned int dTime);
	void onHit(EntityType entityType, EntityRole entityRole);
};


#endif //BATTLECITY_IRONWALL_H
