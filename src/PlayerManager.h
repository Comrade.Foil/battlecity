#ifndef BATTLECITY_PLAYERMANAGER_H
#define BATTLECITY_PLAYERMANAGER_H

#include <vector>
#include "globals.h"
#include "utils.h"
#include "Resources.h"
#include "EntityPool.h"
#include "Entities/Tank.h"

class PlayerManager {
private:
	unsigned int tanksLeft;
	Resources *res;
	EntityPool *ep;
	shared_ptr<Tank> tank;
	void spawnPlayerOnRandomTile();
public:
	PlayerManager(Resources &res, EntityPool &ep, unsigned int tanksLeft);
	~PlayerManager();
	void setMoving(FRKey lastPressed);
	void fire();
	void update(unsigned int dTime);
	bool isDead();
	
};


#endif //BATTLECITY_PLAYERMANAGER_H
