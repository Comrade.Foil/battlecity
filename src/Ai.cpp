#include "Ai.h"

Ai::Ai(Resources &res, EntityPool &ep, unsigned int x, unsigned int y) {
	this->tank = make_shared<Tank>(res, "green", ENEMY, x, y);
	this->ep = &ep;
	this->res = &res;
	this->destination = {(int)this->tank->boundingBox.x, (int)this->tank->boundingBox.y};
	this->tickCounter = 0;
}

bool Ai::update(const unsigned int &dTime) {
	this->tickCounter += dTime;
	
	Vec2i currentPos = {(int)this->tank->boundingBox.x, (int)this->tank->boundingBox.y};
	if (currentPos == this->destination) {
		this->tank->direction = chooseNextDirection();
		this->destination = chooseNextDestination(this->tank->direction);
	}
	this->tank->moving = true;
	if (this->tickCounter > AI_FIRE_LIMITER) {
		this->tank->fire(*this->res, *ep);
		this->tickCounter = 0;
	}
	
	return false;
}

Vec2i Ai::chooseNextDirection() {
	Vec2i possibleDirections[4] = {Vec2i{1, 0}, Vec2i{-1, 0}, Vec2i{0, 1}, Vec2i{0, -1}};
	vector<Vec2i> validDirections;
	for (auto direction : possibleDirections) {
		Vec2i tankPosition = getWorldToTilePosition(
			this->tank->boundingBox.x, this->tank->boundingBox.y
		);
		Vec2i nextCell = tankPosition + direction;
		
		if (isFreeCell(nextCell.x, nextCell.y)) {
			validDirections.push_back(direction);
		}
	}
	int index = randomInRange(0, (int) validDirections.size() - 1);
	Vec2i choosenDirection = validDirections[index];
	return choosenDirection;
}

Vec2i Ai::chooseNextDestination(Vec2i offset) {
	vector<Vec2i> validDestinations;
	
	Vec2i tankCell = getWorldToTilePosition(this->tank->boundingBox.x, this->tank->boundingBox.y);
	Vec2i nextCell = tankCell + offset;
	while (isFreeCell(nextCell.x, nextCell.y)) {
		validDestinations.push_back(getTileToWorldPosition((int) nextCell.x, (int) nextCell.y));
		nextCell = nextCell + offset;
	}
	
	
	int ind = randomInRange(0, (int) validDestinations.size() - 1);
	Vec2i chosenDestination = validDestinations[ind];
	
	return {
		chosenDestination.x,
		chosenDestination.y
	};
}

bool Ai::isFreeCell(unsigned int x, unsigned int y) {
	if (isOutOfBoundsTile(x, y)) {
		return false;
	}
	
	vector<EntityID> dwellers = this->ep->getCellDwellers(x, y);
	
	for (auto entityId : dwellers) {
		shared_ptr<GameEntity> e = this->ep->getEntity(entityId);
		if (e == nullptr) {
			return false;
		}
		if (e->type & (B_WALL | M_WALL | EAGLE)) {
			return false;
		}
	}
	
	return true;
}

bool Ai::isRemoved() {
	return this->tank->removed;
}
