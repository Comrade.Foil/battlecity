#ifndef BATTLECITY_AIMANAGER_H
#define BATTLECITY_AIMANAGER_H

#include <vector>
#include "globals.h"
#include "utils.h"
#include "Resources.h"
#include "EntityPool.h"
#include "Ai.h"

class AiManager {
private:
	Resources *res;
	EntityPool *ep;
	vector<shared_ptr<Ai>> ais;
	unsigned int aiMaxAmount;
	unsigned int tanksLeft;
	unsigned int tickCounter;
	void spawnAiOnRandomTile();
public:
	AiManager(Resources &res, EntityPool &ep, unsigned int aiMaxAmount, unsigned int tanksLeft);
	~AiManager();
	void update(unsigned int dTime);
	bool isDead();
	
};


#endif //BATTLECITY_AIMANAGER_H
