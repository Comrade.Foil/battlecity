#ifndef BATTLECITY_ENTITYPOOL_H
#define BATTLECITY_ENTITYPOOL_H

#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <vector>
#include <memory>
#include "Framework.h"
#include "Entities/GameEntity.h"
#include "Entities/Animation.h"
#include "Resources.h"
#include "globals.h"
#include "utils.h"

using namespace std;
using namespace cnst;

class EntityPool {
public:
	map<EntityID, shared_ptr<GameEntity>> data;
	vector<EntityID> collisionGrid[TILES_NUM_X][TILES_NUM_Y];
	Resources *res;
	
	EntityPool(Resources &res);
	~EntityPool();
	
	void addEntity(shared_ptr<GameEntity> entity);
	void removeEntity(const EntityID &id);
	shared_ptr<GameEntity> getEntity(const EntityID &id);
	void update(const unsigned int &dTime);
	void updateCollisionGrid();
	vector<EntityID> getNeighbours(const EntityID &id);
	vector<Vec2i> getFreeCells();
	vector<EntityID> getCellDwellers(int x, int y);
	
	void draw();
	
	void destroyRemovedEntities();
	
	void destroy();
};


#endif //BATTLECITY_ENTITYPOOL_H
