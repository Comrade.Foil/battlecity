#include "PlayerManager.h"

KeyDirectionMap_t keyDirectionMap = {
	{FRKey::LEFT,  {-1, 0}},
	{FRKey::RIGHT, {1,  0}},
	{FRKey::UP,    {0,  -1}},
	{FRKey::DOWN,  {0,  1}},
};

PlayerManager::PlayerManager(Resources &res, EntityPool &ep, unsigned int tanksLeft) {
	this->res = &res;
	this->ep = &ep;
	this->tanksLeft = tanksLeft;
	spawnPlayerOnRandomTile();
}

void PlayerManager::update(unsigned int dTime) {
	if (tanksLeft > 0 && tank->removed) {
		tank.reset();
		spawnPlayerOnRandomTile();
		tanksLeft--;
	}
	
}

void PlayerManager::fire() {
	tank->fire(*res, *ep);
}

void PlayerManager::setMoving(FRKey lastPressed) {
	tank->direction = keyDirectionMap[lastPressed];
	tank->moving = true;
}

bool PlayerManager::isDead() {
	return tanksLeft == 0;
}

void PlayerManager::spawnPlayerOnRandomTile() {
	vector<Vec2i> freeTiles = this->ep->getFreeCells();
	Vec2i randomTile = freeTiles[randomInRange(0, (int) freeTiles.size() - 1)];
	Vec2i worldPosition = getTileToWorldPosition(randomTile.x, randomTile.y);
	tank = make_shared<Tank>(*res, "yellow", PLAYER, (unsigned int) worldPosition.x, (unsigned int) worldPosition.y);
	ep->addEntity(tank);
	this->ep->updateCollisionGrid();
}

PlayerManager::~PlayerManager() {
	tank.reset();
}