#ifndef BATTLECITY_COLLIDER_H
#define BATTLECITY_COLLIDER_H

#include <vector>
#include <iostream>
#include <iterator>
#include "globals.h"
#include "Entities/GameEntity.h"
#include "EntityPool.h"
#include "utils.h"
#include "SDL2/SDL.h"

using namespace std;
using namespace cnst;

class Collider {

public:
	void checkCollisions(EntityPool &ep) {
		
		for (auto it = ep.data.begin(); it != ep.data.end(); it++) {
			shared_ptr<GameEntity> e1 = it->second;
			vector<EntityID> neighbours = ep.getNeighbours(e1->id);
			
			for (auto jt = neighbours.begin(); jt != neighbours.end(); jt++) {
				shared_ptr<GameEntity> e2 = ep.getEntity(*jt);
				Rect box1 = e1->boundingBox;
				Rect box2 = e2->boundingBox;
				
				Vec2 half1 = Vec2{box1.w / 2, box1.h / 2};
				Vec2 hlaf2 = Vec2{box2.w / 2, box2.h / 2};
				Vec2 c1 = e1->getCenter();
				Vec2 c2 = e2->getCenter();
				double distanceX = abs(c2.x - c1.x);
				double distanceY = abs(c2.y - c1.y);
				
				if (distanceX < half1.x + hlaf2.x && distanceY < half1.y + hlaf2.y) {
					resolveCollision(e1, e2);
				}
			}
		}
	};
	
private:
	void resolveCollision(const shared_ptr<GameEntity> &e1, const shared_ptr<GameEntity> &e2) {
		if ((e1->type & TANK) && (e2->type & (B_WALL | M_WALL))) {
			resolveTankWallCollision(e1, e2);
		} else if ((e1->type & (B_WALL | M_WALL)) && (e2->type & TANK)) {
			resolveTankWallCollision(e2, e1);
		}
		
		e1->onHit(e2->type, e2->role);
	}
	
	void resolveTankWallCollision(shared_ptr<GameEntity> tank, shared_ptr<GameEntity> wall) {
#ifdef DEBUG_INFO
		cout << "tank: " << tank->boundingBox.x << " ; " << tank->boundingBox.y << endl;
		cout << "wall: " << wall->boundingBox.x << " ; " << wall->boundingBox.y << endl;
#endif
		
		Vec2 minDistance = Vec2{
			tank->boundingBox.w / 2 + wall->boundingBox.w / 2,
			tank->boundingBox.h / 2 + wall->boundingBox.h / 2
		};
		Vec2 direction = wall->getCenter() - tank->getCenter();
#ifdef DEBUG_INFO
		cout << "direction: " << direction.x << " ; " << direction.y << endl;
#endif
		bool snap = vecLength(direction) > SNAPPING_THRESHOLD;
		Vec2 diff = minDistance - vecAbs(direction);
#ifdef DEBUG_INFO
		cout << "diff: " << diff.x << " ; " << diff.y << endl;
#endif
		Vec2 compensation = snap ?
								 Vec2{diff.x * -sgn(direction.x), diff.y * -sgn(direction.y)} :
								 Vec2{diff.x * -tank->direction.x, diff.y * -tank->direction.y};

#ifdef DEBUG_INFO
		cout << "compensation: " << compensation.x << " ; " << compensation.y << endl;
#endif
		tank->boundingBox = tank->boundingBox + compensation;
	}
	
	void resolveTankEdgeCollision(shared_ptr<GameEntity> tank, shared_ptr<GameEntity> edge) {
		Vec2 minDistance = Vec2{
			tank->boundingBox.w / 2 + edge->boundingBox.w / 2,
			tank->boundingBox.h / 2 + edge->boundingBox.h / 2
		};
		Vec2 diff = minDistance - vecAbs(edge->getCenter() - tank->getCenter());
		
		Vec2 compensation = Vec2{diff.x * -tank->direction.x, diff.y * -tank->direction.y};

		tank->boundingBox = tank->boundingBox + compensation;
	}
	
};


#endif //BATTLECITY_COLLIDER_H
