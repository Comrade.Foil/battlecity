#include <stdlib.h>
#include <map>
#include <vector>
#include <algorithm>
#include <math.h>

#include "Framework.h"
#include "utils.h"

#include "src/Entities/Tank.h"
#include "src/Entities/Eagle.h"
#include "src/Entities/Bullet.h"
#include "src/Entities/BrickWall.h"
#include "src/Entities/MetalWall.h"
#include "src/Entities/Animation.h"

#include "src/EntityPool.h"
#include "src/Resources.h"
#include "src/Collider.h"
#include "src/AiManager.h"
#include "src/PlayerManager.h"


using namespace std;
using namespace cnst;

bool MyKeyState[(int) FRKey::COUNT] = {false};
bool MyMouseState[(int) FRMouseButton::COUNT] = {false};

class MyFramework : public Framework {
private:
	vector<FRKey> keyStates;
	int window_w, window_h;
public:
	EntityPool* ep;
	shared_ptr<Eagle> eagle;
	AiManager *aiManager;
	PlayerManager *playerManager;
	Collider c;
	Resources res;
	unsigned int lastUpdateTime;
	
	MyFramework() : window_w(0), window_h(0) {};
	
	MyFramework(int w, int h) : window_w(w), window_h(h) {};
	
	virtual void PreInit(int &width, int &height, bool &fullscreen) {
		if (window_w > 0 && window_h > 0) {
			width = window_w;
			height = window_h;
		} else {
			width = 800;
			height = 600;
		}
		fullscreen = false;
		srand((unsigned) time(NULL));
	}
	
	virtual bool Init() {
		this->res = Resources();
		this->res.initResources();
		this->c = Collider();
		this->ep = new EntityPool(this->res);
		
		this->initializeGameMap(MAP);
		
		this->playerManager = new PlayerManager(res, *ep, PLAYER_TANKS_NUM);
		this->aiManager = new AiManager(this->res, *ep, AI_MAX_AMOUNT, AI_TANKS_LEFT);
		
		this->lastUpdateTime = getTickCount();
		
		return true;
	}
	
	virtual void Close() {
		eagle.reset();
		ep->destroy();
		res.destroy();
		ep->destroy();
		delete aiManager;
		delete playerManager;
	}
	
	virtual bool Tick() {
		if (playerManager->isDead()) {
			cout << "All player tanks are destroyed. Game over." << endl;
			return true;
		}
		if (eagle->removed) {
			cout << "Headquarters are destroyed. Game over." << endl;
			return true;
		}
		
		if (aiManager->isDead()) {
			cout << "All enemies are destroyed. Victory." << endl;
			return true;
		}
		
		int dTime = getTickCount() - lastUpdateTime;
		
		if (!keyStates.empty()) {
			FRKey lastPressed = keyStates.back();
			playerManager->setMoving(lastPressed);
		}
		
		if (MyMouseState[(int) FRMouseButton::LEFT]) {
			playerManager->fire();
			MyMouseState[(int) FRMouseButton::LEFT] = false;
		}
		
		playerManager->update(dTime);
		aiManager->update(dTime);
		ep->update(dTime);
		ep->updateCollisionGrid();
		c.checkCollisions(*ep);
		drawTestBackground();
		ep->draw();
		
		ep->destroyRemovedEntities();
		
		lastUpdateTime = getTickCount();
		
		return false;
	}
	
	virtual void onMouseMove(int x, int y, int xrelative, int yrelative) {
	
	}
	
	virtual void onMouseButtonClick(FRMouseButton button, bool isReleased) {
		MyMouseState[(int) button] = !isReleased;
	}
	
	virtual void onKeyPressed(FRKey k) {
		MyKeyState[(int) k] = true;
		this->keyStates.push_back(k);
	}
	
	virtual void onKeyReleased(FRKey k) {
		auto it = remove(this->keyStates.begin(), this->keyStates.end(), k);
		this->keyStates.erase(it, this->keyStates.end());
	}
	
	virtual const char *GetTitle() override {
		return "Tanks";
	}
	
	void initializeGameMap(const EntityType (&map)[TILES_NUM_Y][TILES_NUM_Y]) {
		for (int y = 0; y < TILES_NUM_Y; y++) {
			for (int x = 0; x < TILES_NUM_X; x++) {
				switch (map[y][x]) {
					case B_WALL:
						this->ep->addEntity(make_shared<BrickWall>(res, x * TILE_WIDTH, y * TILE_WIDTH));
						break;
					case M_WALL:
						this->ep->addEntity(make_shared<MetalWall>(res, x * TILE_WIDTH, y * TILE_WIDTH));
						break;
					case EAGLE:
						this->eagle = make_shared<Eagle>(res, x * TILE_WIDTH, y * TILE_WIDTH);
						this->ep->addEntity(this->eagle);
						break;
					default:
						break;
				}
			}
		}
		this->ep->updateCollisionGrid();
	}
};

char *getParameterValue(int argc, char *argv[], char *param) {
	char *value = new char[18];
	
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], param) == 0) {
			if (i + 1 < argc) {
				strcpy(value, argv[i + 1]);
				cout << value << endl;
				return value;
			} else {
				return nullptr;
			}
		}
	}
	
	return nullptr;
}

Vec2 parseWindowDimensions(char *windowSize) {
	Vec2 dim = {0, 0};
	
	dim.x = atoi(strtok(windowSize, "x"));
	dim.y = atoi(strtok(NULL, "x"));
	
	return dim;
}

int main(int argc, char *argv[]) {
	MyFramework *fw;
	char *paramValue = getParameterValue(argc, argv, "-window");
	
	if (paramValue != nullptr) {
		Vec2 dim = parseWindowDimensions(paramValue);
		fw = new MyFramework(dim.x, dim.y);
	} else {
		fw = new MyFramework();
	}
	
	return run(fw);
}
